<?php
get_header();

atheus_render_page_header( 'portfolio' );
?>
	<main>
		<?php
		while ( have_posts() ) {
			the_post();
			the_content();
		}
		?>
	</main>
<?php
get_footer();
