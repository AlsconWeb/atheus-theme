=== Atheus ===
Author: Drection
Requires at least: 5.0
Tested up to: 5.2
Requires PHP: 5.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Modern Beautiful Creative Portfolio for Freelancers & Agencies WordPress Theme


== Description ==

Atheus is a beautiful agency website for all kind of creative and portfolio purposes. If you want to present your agency in best look than you might use Atheus to create your website very easily and quickly. Based on WP Baker and all files and codes has been well organized.


== Changelog ==

= 1.0 =
* Initial Release

