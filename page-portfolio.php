<?php
/**
 * Template Name: Portfolio
 *
 */

get_header();

atheus_render_page_header( 'page' );

$animate       = get_field( 'animate' );
$wrapper_class = [];

if ( $animate === 'yes' ) {
	$animation_type = get_field( 'animation_type' );

	$wrapper_class[] = 'wow';
	$wrapper_class[] = $animation_type;
}

$filter_term = false;

if ( get_field( 'tags' ) && get_field( 'tags' ) > 0 ) {
	$filter_term = true;
}

$wrapper_class = implode( ' ', $wrapper_class );

while ( have_posts() ) {
	the_post();

	the_content();
}

get_footer();
